'use strict';
const path = require('path');
const webpack = require('webpack');
const CommonsChunkPlugin = webpack.optimize.CommonsChunkPlugin;
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';

module.exports = {
	watch: isDevelopment,
	context: __dirname + '/',
	entry:  {
		home: ['./dev/pages/home/home.js'],
		common: ['./dev/common/js/common.js']
	},
	output:  {
		path: __dirname + '/build',
		publicPath: '/',
		filename: '[name]/[name].js'
	},

	resolve: {
		extensions: ['', '.js'],
		modulesDirectories: [
			'node_modules',
			'bower_components'
		]
	},

	module: {
		loaders: [
			{
				test:   /\.js$/,
				exclude: /\/node_modules\//,
				loader: "babel?presets[]=es2015"
			}
		]
	},

	plugins: [
		new CommonsChunkPlugin({
			name: "common",
			filename: "common.js"
		}),
		new webpack.ProvidePlugin({
			$: "jquery/src/jquery",
			jQuery: "jquery/src/jquery",
			"window.jQuery": "jquery/src/jquery"
		}),
		new webpack.optimize.UglifyJsPlugin({
			compress: {
				warnings:     false,
				unsafe:       true
			}
		}),
		new webpack.NoErrorsPlugin()
	],

	parserOptions: {
		"ecmaFeatures": {
			"modules": true
		}
	}
};